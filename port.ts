// import { controller, httpGet, queryParam } from "inversify-express-utils";
// import { inject } from "inversify";
// import { FirstInput, SecondInput } from "./inputs";

// @controller('/first')
// export class FirstPort {
//     constructor(@inject(FirstInput) private input: FirstInput) { }

//     @httpGet('/')
//     get(@queryParam('userId') userId: string) {
//         this.input.execute({ userId });
//         return 'first url';
//     }
// }

// @controller('/second')
// export class SecondPort {
//     constructor(@inject(SecondInput) private input: SecondInput) { }

//     @httpGet('/')
//     get(@queryParam('userId') userId: string, @queryParam('otherParam') otherParam: string) {
//         this.input.execute({ userId, otherParam });
//         return 'second url';
//     }
// }