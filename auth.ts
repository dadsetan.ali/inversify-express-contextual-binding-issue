import { inject, injectable } from "inversify";
import TYPES from "./types";

export type AuthenticationParams = {
    userId: string,
}

export interface NextInteractor<T>{
    execute(params: T): void;
}

@injectable()
export class AuthenticationInteractor<T extends AuthenticationParams>{
    constructor(@inject(TYPES.NEXT_INTERACTOR) private nextInteractor: NextInteractor<T>){}

    execute(params: T){
        console.log(`continuing as user with userId ${params.userId}`);
        this.nextInteractor.execute(params);
    }
}