import "reflect-metadata";
import { Container } from 'inversify';
import { NextInteractor, AuthenticationInteractor } from "./auth";
// import { FirstParams, FirstInteractor} from "./nextInteractors";
import { FirstParams, FirstInteractor, SecondParams, SecondInteractor } from "./nextInteractors";
import TYPES from "./types";
import { FirstInput, SecondInput } from "./inputs";
// import { FirstInput} from "./inputs";

let container = new Container();

container.bind<AuthenticationInteractor<any>>(TYPES.AUTHENTICATION_INTERACTOR).to(AuthenticationInteractor);

container.bind<NextInteractor<FirstParams>>(TYPES.NEXT_INTERACTOR).to(FirstInteractor).whenAnyAncestorIs(FirstInput);
container.bind<NextInteractor<SecondParams>>(TYPES.NEXT_INTERACTOR).to(SecondInteractor).whenAnyAncestorIs(SecondInput);

container.bind<FirstInput>(FirstInput).toSelf();
container.bind<SecondInput>(SecondInput).toSelf();

export default container;