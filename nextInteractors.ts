import { NextInteractor } from "./auth";
import { injectable } from "inversify";

export type FirstParams = {
    userId: string
}

@injectable()
export class FirstInteractor implements NextInteractor<FirstParams>{
    execute(params: FirstParams) {
        console.log(`printing from first interactor ${params.userId}`)
    }
}


export type SecondParams = {
    userId: string,
    otherParam: string,
}

@injectable()
export class SecondInteractor implements NextInteractor<SecondParams>{
    execute(params: SecondParams) {
        console.log(`printing from second interactor, ${params.userId}, ${params.otherParam}`)
    }
}