import { AuthenticationInteractor } from "./auth";
// import { FirstParams} from "./nextInteractors";
import { FirstParams, SecondParams } from "./nextInteractors";
import { inject } from "inversify";
import TYPES from "./types";
import { controller, httpGet, queryParam } from "inversify-express-utils";
// import getDecorators from 'inversify-inject-decorators';

// import container from './bootstrap';

// let { lazyInject } = getDecorators(container);

@controller('/first')
export class FirstInput {
    constructor(@inject(TYPES.AUTHENTICATION_INTERACTOR) private authInteractor: AuthenticationInteractor<FirstParams>) {}

    @httpGet('/')
    get(@queryParam('userId') userId: string) {
        this.execute({ userId });
        return 'first url';
    }

    execute(params: FirstParams) {
        this.authInteractor.execute(params);
    }
}

@controller('/second')
export class SecondInput {
    constructor(@inject(TYPES.AUTHENTICATION_INTERACTOR) private authInteractor: AuthenticationInteractor<SecondParams>) { }

    @httpGet('/')
    get(@queryParam('userId') userId: string, @queryParam('otherParam') otherParam: string) {
        this.execute({ userId, otherParam });
        return 'second url';
    }

    execute(params: SecondParams) {
        this.authInteractor.execute(params);
    }
}