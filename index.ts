import container from "./bootstrap";
// import { SecondInput, FirstInput } from "./inputs";
import './port';
import {InversifyExpressServer} from 'inversify-express-utils';
// import { GenericInteractor, /*GenericRepository */ } from "./abstractAndInterfaces";
// import { /*Entity,*/ Interactor } from "./implementations";
// import TYPES from "./types";

// let interactor = container.get<GenericInteractor<Entity>>(Interactor);
// let firstInput = container.get<FirstInput>(FirstInput);
// firstInput.execute({ userId: 'first user id' });

// const secondInput = container.get<SecondInput>(SecondInput);
// secondInput.execute({ userId: 'second user id', otherParam: 'other param' });

let server = new InversifyExpressServer(container);

let serverInstance = server.build();
let port = process.env.PORT;
if(port === '' || port === undefined){
    console.log('no PORT env!!');
    process.exit(1);
}
serverInstance.listen(port);